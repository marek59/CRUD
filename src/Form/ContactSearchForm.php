<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Search form.
 */
class ContactSearchForm extends AbstractType {

  /**
   * Build form method.
   *
   * @param \Symfony\Component\Form\FormBuilderInterface $builder
   *   Builder object.
   * @param array $options
   *   Options.
   */
  public function buildForm(FormBuilderInterface $builder, array $options): void {
    $builder
      ->add('phone_readable')
      ->add('Search', SubmitType::class);
  }

}
