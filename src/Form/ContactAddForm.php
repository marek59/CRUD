<?php

namespace App\Form;

use App\Entity\ContactEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Contact add and edit form.
 */
class ContactAddForm extends AbstractType {

  /**
   * Build form method.
   *
   * @param \Symfony\Component\Form\FormBuilderInterface $builder
   *   $builder object.
   * @param array $options
   *   Options.
   */
  public function buildForm(FormBuilderInterface $builder, array $options): void {
    $builder
      ->add('phone_readable')
      ->add('user')
      ->add('Submit', SubmitType::class);
  }

  /**
   * Set default entity as ContactEntity.
   *
   * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
   *   Options resolver.
   */
  public function configureOptions(OptionsResolver $resolver): void {
    $resolver->setDefaults([
      'data_class' => ContactEntity::class,
    ]);
  }

}
