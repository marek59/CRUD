<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for SQL dump generator.
 */
class SqlController extends AbstractController {

  /**
   * Generate list of phone numbers as SQL query.
   *
   * @param int $count
   *   Url parameter with amout of items.
   *
   * @Route("/generate/{count}", name="sql_contacts")
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response object.
   */
  public function generate(int $count): Response {
    // Cached phone numbers to skip duplicates.
    $cache = [];
    // Generate wrapper.
    $export = 'DROP TABLE IF EXISTS contact_entity;' . PHP_EOL;
    $export .= 'CREATE TABLE contact_entity (phone_unified INT NOT NULL, phone_readable VARCHAR(255) NOT NULL, user VARCHAR(255) NOT NULL, PRIMARY KEY(phone_unified)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;' . PHP_EOL;
    $export .= 'INSERT INTO contact_entity (phone_unified, phone_readable, user)' . PHP_EOL . 'VALUES' . PHP_EOL;
    while (count($cache) < $count) {
      // Generate row.
      $phone_readable = '+420 ' . str_pad(rand(0, 999), 3, '0', STR_PAD_LEFT) . ' ' . str_pad(rand(0, 999), 3, '0', STR_PAD_LEFT) . ' ' . str_pad(rand(0, 999), 3, '0', STR_PAD_LEFT);
      $phone_unified = trim($phone_readable);
      $phone_unified = preg_replace('/\s/', '', $phone_unified);
      $phone_unified = preg_replace('/^\+420/', '', $phone_unified);
      // Can not create duplicate primary keys.
      if (array_key_exists($phone_unified, $cache)) {
        continue;
      }
      else {
        $cache[$phone_unified] = 1;
      }
      $user = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 10);
      // Add row to export.
      $export .= '("' . $phone_unified . '","' . $phone_readable . '","' . $user . '")';
      if (count($cache) == $count) {
        $export .= ';';
      }
      else {
        $export .= ',';
      }
      $export .= PHP_EOL;
    }

    return $this->render('sql/generate.html.twig', [
      'export' => $export,
    ]);
  }

}
