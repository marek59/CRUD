<?php

namespace App\Controller;

use App\Entity\ContactEntity;
use App\Form\ContactAddForm;
use App\Form\ContactSearchForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatableMessage;

/**
 * Basic CRUD controller for contacts.
 */
class ContactsController extends AbstractController {

  /**
   * App homepage.
   *
   * @Route("/contacts", name="index")
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response object.
   */
  public function index(): Response {
    return $this->render('contacts/index.html.twig', [
      'controller_name' => 'ContactsController',
    ]);
  }

  /**
   * New contact form page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @Route("/contacts/new", name="new_contact")
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response object.
   */
  public function new(Request $request): Response {
    $locale = $request->getLocale();
    // Get contact and form entities.
    $contact = new ContactEntity();
    $contact_form = $this->createForm(ContactAddForm::class, $contact);
    $contact_form->handleRequest($request);
    if ($contact_form->isSubmitted()) {
      if ($contact_form->isValid()) {
        // Create contact.
        $unified_phone = $this->createOrUpdateContact($contact, 'create');
      }
      else {
        $this->addFlash('error', new TranslatableMessage('Invalid contact data'));
      }
      if (!empty($unified_phone) && is_numeric($unified_phone)) {
        // Go to detail.
        return new RedirectResponse('/' . $locale . '/contacts/' . $unified_phone);
      }
    }
    return $this->render('contacts/new_contact.html.twig', [
      'form' => $contact_form->createView(),
    ]);
  }

  /**
   * Update form page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   * @param int $unified_phone
   *   Phone number url parameter.
   *
   * @Route("/contacts/{unified_phone}/update", name="update_contact")
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response object.
   */
  public function update(Request $request, int $unified_phone): Response {
    $locale = $request->getLocale();
    // Get contact and form entities.
    $contact = $this->getDoctrine()->getRepository(ContactEntity::class)->find($unified_phone);
    // Contact not found.
    if (empty($contact)) {
      throw new NotFoundHttpException('Contact not found');
    }
    $contact_form = $this->createForm(ContactAddForm::class, $contact);
    $contact_form->handleRequest($request);
    if ($contact_form->isSubmitted()) {
      if ($contact_form->isValid()) {
        // Update contact.
        $unified_phone = $this->createOrUpdateContact($contact, 'update');
      }
      else {
        $this->addFlash('error', new TranslatableMessage('Invalid contact data'));
      }
      if (is_numeric($unified_phone)) {
        // Go back to detail.
        return new RedirectResponse('/' . $locale . '/contacts/' . $unified_phone);
      }
      else {
        // Go back to homepage.
        return new RedirectResponse('/' . $locale . '/contacts');
      }
    }
    return $this->render('contacts/new_contact.html.twig', [
      'form' => $contact_form->createView(),
    ]);
  }

  /**
   * Save contact entity and notify user.
   *
   * @param \App\Entity\ContactEntity $contact
   *   Contact entity.
   * @param string $op
   *   Operation - update or create.
   *
   * @return string|null
   *   Entity id on success, otherwise null.
   */
  private function createOrUpdateContact(ContactEntity $contact, string $op) {
    $contact->setPhoneUnified($contact->generatePhoneUnified($contact->getPhoneReadable()));
    try {
      // Save contact.
      $entity_manager = $this->getDoctrine()->getManager();
      $entity_manager->persist($contact);
      $entity_manager->flush();
      if ($op == 'update') {
        $this->addFlash('notice', new TranslatableMessage('Contact updated'));
      }
      elseif ($op == 'create') {
        $this->addFlash('notice', new TranslatableMessage('Contact created'));
      }
      return $contact->getPhoneUnified();
    }
    catch (\Exception $e) {
      $this->addFlash('error', $e->getMessage());
      return NULL;
    }
  }

  /**
   * Delete page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   * @param int $unified_phone
   *   Phone number url parameter.
   *
   * @Route("/contacts/{unified_phone}/delete", name="delete_contact")
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response object.
   */
  public function delete(Request $request, int $unified_phone): Response {
    $locale = $request->getLocale();
    // Get contact entity.
    $contact = $this->getDoctrine()->getRepository(ContactEntity::class)->find($unified_phone);
    // Contact not found.
    if (empty($contact)) {
      throw new NotFoundHttpException('Contact not found');
    }
    $entity_manager = $this->getDoctrine()->getManager();
    $entity_manager->remove($contact);
    $entity_manager->flush();
    $this->addFlash('notice', new TranslatableMessage('Contact deleted'));
    return new RedirectResponse('/' . $locale . '/contacts');
  }

  /**
   * Contact detail page.
   *
   * @param int $unified_phone
   *   Phone number url parameter.
   *
   * @Route("/contacts/{unified_phone}", name="contact_detail")
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response object.
   */
  public function detail(int $unified_phone): Response {
    // Get contact entity.
    $contact = $this->getDoctrine()->getRepository(ContactEntity::class)->find($unified_phone);
    // Contact not found.
    if (empty($contact)) {
      throw new NotFoundHttpException('Contact not found');
    }
    return $this->render('contacts/contact.html.twig', [
      'user' => $contact->getUser(),
      'phone' => $contact->getPhoneReadable(),
      'unified_phone' => $unified_phone,
    ]);
  }

  /**
   * Search page with form and results.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @Route("/search", name="search")
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response object.
   */
  public function search(Request $request): Response {
    $contact = NULL;
    $search_form = $this->createForm(ContactSearchForm::class);
    $search_form->handleRequest($request);
    // Display results if form is submitted.
    if ($search_form->isSubmitted()) {
      if ($search_form->isValid()) {
        $data = $search_form->getData();
        if (!empty($data['phone_readable'])) {
          $phone_unified = ContactEntity::generatePhoneUnified($data['phone_readable']);
          $contact = $this->getDoctrine()->getRepository(ContactEntity::class)->find($phone_unified);
        }
      }
    }

    return $this->render('contacts/search.html.twig', [
      'form' => $search_form->createView(),
      'contact' => $contact,
    ]);
  }

}
