<?php

namespace App\Repository;

use App\Entity\ContactEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ContactEntity>
 *
 * @method ContactEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactEntity[]    findAll()
 * @method ContactEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactEntityRepository extends ServiceEntityRepository {

  /**
   *
   */
  public function __construct(ManagerRegistry $registry) {
    parent::__construct($registry, ContactEntity::class);
  }

  /**
   *
   */
  public function add(ContactEntity $entity, bool $flush = FALSE): void {
    $this->getEntityManager()->persist($entity);

    if ($flush) {
      $this->getEntityManager()->flush();
    }
  }

  /**
   *
   */
  public function remove(ContactEntity $entity, bool $flush = FALSE): void {
    $this->getEntityManager()->remove($entity);

    if ($flush) {
      $this->getEntityManager()->flush();
    }
  }

  // /**
  //     * @return ContactEntity[] Returns an array of ContactEntity objects
  //     */
  //    public function findByExampleField($value): array
  //    {
  //        return $this->createQueryBuilder('c')
  //            ->andWhere('c.exampleField = :val')
  //            ->setParameter('val', $value)
  //            ->orderBy('c.id', 'ASC')
  //            ->setMaxResults(10)
  //            ->getQuery()
  //            ->getResult()
  //        ;
  //    }
  //    public function findOneBySomeField($value): ?ContactEntity
  //    {
  //        return $this->createQueryBuilder('c')
  //            ->andWhere('c.exampleField = :val')
  //            ->setParameter('val', $value)
  //            ->getQuery()
  //            ->getOneOrNullResult()
  //        ;
  //    }
}
