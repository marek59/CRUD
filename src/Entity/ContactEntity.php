<?php

namespace App\Entity;

use App\Repository\ContactEntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Contact Entity.
 *
 * @ORM\Entity(repositoryClass=ContactEntityRepository::class)
 */
class ContactEntity {

  /**
   * Phone number casted to int as id.
   *
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $phone_unified;

  /**
   * Vidible phone number.
   *
   * @ORM\Column(type="string", length=255)
   * @Assert\NotBlank
   */
  private $phone_readable;

  /**
   * Username.
   *
   * @ORM\Column(type="string", length=255)
   * @Assert\NotBlank
   */
  private $user;

  /**
   * Basic getter.
   */
  public function getPhoneUnified(): ?string {
    return $this->phone_unified;
  }

  /**
   * Basic setter.
   */
  public function setPhoneUnified(string $phone_unified): self {
    $this->phone_unified = $phone_unified;

    return $this;
  }

  /**
   * Basic getter.
   */
  public function getPhoneReadable(): ?string {
    return $this->phone_readable;
  }

  /**
   * Basic setter.
   */
  public function setPhoneReadable(string $phone_readable): self {
    $this->phone_readable = $phone_readable;

    return $this;
  }

  /**
   * Basic getter.
   */
  public function getUser(): ?string {
    return $this->user;
  }

  /**
   * Basic setter.
   */
  public function setUser(string $user): self {
    $this->user = $user;

    return $this;
  }

  /**
   * Generate unified phone number from readable phone number.
   *
   * @param string $phone
   *   Readable phone number.
   *
   * @return int
   *   Unified phone number.
   */
  public static function generatePhoneUnified(string $phone): int {
    $phone = trim($phone);
    $phone = preg_replace('/\s/', '', $phone);
    $phone = preg_replace('/^\+420/', '', $phone);
    return (int) $phone;
  }

  /**
   * Validate phone number.
   *
   * @param \Symfony\Component\Validator\Context\ExecutionContextInterface $context
   * @param $payload
   *
   * @Assert\Callback
   */
  public function validate(ExecutionContextInterface $context, $payload) {
    $unified_phone = self::generatePhoneUnified($this->getPhoneReadable());
    if (empty($unified_phone) || !is_numeric($unified_phone)) {
      $context->buildViolation(new TranslatableMessage('Phone is not valid'))
        ->atPath('phone_readable')
        ->addViolation();
    }
  }

}
